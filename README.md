# Backend

## How to start the project
1. First you need running PSQL database.

| Key      | Default Value    | Env Variable |
| -------- | ---------------- | ------------ |
| Host     | localhost:5432   | DB_HOST      |
| Name     | hackathon_system | DB_NAME      |
| Username | hackathon_system | DB_USER      |
| Password | hackathon_system | DB_PASS      |

2. This project requires keycloack. Currently, we have up and running instance
   which means you only need internet connection to be able to interact with it.

3. Run the application via `./gradlew bootRun` or whatever preferred way you have
   to run SpringBoot applications.

## Endpoints

#### GET `/hackathons`

###### Response

```json
[
  {
    "id": 0,
    "name": "STRING",
    "location": "STRING",
    "startDate": "DATE",
    "endDate": "DATE",
    "creator": 0
  }
]
```

#### POST `/hackathons`

###### Request

```json
{
  "name": "STRING",
  "location": "STRING",
  "startDate": "DATE",
  "endDate": "DATE"
}
```

###### Response

Headers: Location? (projectId)

#### PUT `/hackathons/{hackathonId}`

Permissions: only creator can use this

###### Request

```json
{
  "name": "STRING",
  "location": "STRING",
  "startDate": "DATE",
  "endDate": "DATE"
}
```

#### GET `/hackathons/{hackathonId}/projects`

###### Response

```json
[
  {
    "id": 0,
    "name": "STRING",
    "description": "STRING",
    "hackathonId": 0,
    "technologies": [
      0,
      1,
      2
    ]
  }
]
```

#### POST `/projects`

###### Response

```json
{
  "id": 0,
  "name": "STRING",
  "description": "STRING",
  "hackathonId": 0,
  "technologies": [
    0,
    1,
    2
  ]
}
```

#### PUT `/projects/{projectId}`

Permissions: only creator can use this

###### Response

```json
{
  "id": 0, 
  "name": "STRING",
  "description": "STRING",
  "hackathonId": 0, 
  "technologies": [
    0,
    1,
    2
  ]
}
```

#### GET `/projects/{projectId}`

###### Response

```json
{
  "id": 0,
  "name": "STRING",
  "description": "STRING",
  "hackathonId": 0,
  "technologies": [
    0,
    1,
    2
  ]
}
```
