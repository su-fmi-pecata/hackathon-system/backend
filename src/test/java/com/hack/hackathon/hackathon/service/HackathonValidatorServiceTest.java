package com.hack.hackathon.hackathon.service;

import com.hack.hackathon.hackathon.HackathonException;
import com.hack.hackathon.hackathon.model.HackathonRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {HackathonValidatorService.class})
class HackathonValidatorServiceTest
{

	@Autowired
	private HackathonValidatorService hackathonValidatorService;

	@Test
	void testMissingField()
	{
		final String name = "name";
		final String location = "location";
		final LocalDateTime startDate = LocalDateTime.now();
		final LocalDateTime endDate = LocalDateTime.now().plusNanos(1);

		testNpeIsThrown(new HackathonRequest(null, null, null, null));
		testNpeIsThrown(new HackathonRequest(name, null, null, null));
		testNpeIsThrown(new HackathonRequest(name, location, null, null));
		testNpeIsThrown(new HackathonRequest(name, location, startDate, null));
		testNpeIsThrown(new HackathonRequest(name, location, null, endDate));
		testNpeIsThrown(new HackathonRequest(name, location, null, endDate));
		testNpeIsThrown(new HackathonRequest(name, null, startDate, endDate));
		testNpeIsThrown(new HackathonRequest(null, location, startDate, endDate));

		validate(new HackathonRequest(name, location, startDate, endDate));
	}

	@Test
	void testDateRange()
	{
		final String name = "name";
		final String location = "location";
		testHackathonExceptionIsThrown(new HackathonRequest(name, location, LocalDateTime.MAX, LocalDateTime.MIN));
		final LocalDateTime now = LocalDateTime.now();

		testHackathonExceptionIsThrown(new HackathonRequest(name, location, now, now.minusDays(1)));
		testHackathonExceptionIsThrown(new HackathonRequest(name, location, now, now.minusNanos(1)));

		validate(new HackathonRequest(name, location, now, now));
		validate(new HackathonRequest(name, location, now, now.plusNanos(1)));
		validate(new HackathonRequest(name, location, now, now.plusDays(1)));
		validate(new HackathonRequest(name, location, now, now.plusDays(2)));
	}

	private void validate(final HackathonRequest request)
	{
		hackathonValidatorService.validate(request);
	}

	private void testNpeIsThrown(final HackathonRequest request)
	{
		assertThrows(NullPointerException.class, () -> validate(request));
	}

	private void testHackathonExceptionIsThrown(final HackathonRequest request)
	{
		assertThrows(HackathonException.class, () -> validate(request));
	}
}