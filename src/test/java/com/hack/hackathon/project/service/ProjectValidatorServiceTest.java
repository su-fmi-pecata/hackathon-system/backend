package com.hack.hackathon.project.service;

import com.hack.hackathon.project.ProjectException;
import com.hack.hackathon.project.model.ProjectRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.parameters.P;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ProjectValidatorService.class})
class ProjectValidatorServiceTest
{

	@Autowired
	private ProjectValidatorService projectValidatorService;

	@Test
	void testMissingField()
	{
		final String name = "name";
		final String description = "description";
		final Long hackathonId = 1L;
		final Set<Long> technologies = Set.of(2L);

		testNpeIsThrown(new ProjectRequest(null, null, null, null));
		testNpeIsThrown(new ProjectRequest(name, null, null, null));
		testNpeIsThrown(new ProjectRequest(name, description, null, null));
		testNpeIsThrown(new ProjectRequest(name, description, hackathonId, null));
		testNpeIsThrown(new ProjectRequest(name, description, null, technologies));
		testNpeIsThrown(new ProjectRequest(name, null, hackathonId, technologies));
		testNpeIsThrown(new ProjectRequest(null, description, hackathonId, technologies));
		testNpeIsThrown(new ProjectRequest(null, null, hackathonId, technologies));
		testNpeIsThrown(new ProjectRequest(null, description, null, technologies));
		testNpeIsThrown(new ProjectRequest(null, description, hackathonId, null));

		validate(new ProjectRequest(name, description, hackathonId, technologies));
	}

	@Test
	void testTechnologies() {
		final String name = "name";
		final String description = "description";
		final Long hackathonId = 1L;

		testProjectExceptionIsThrown(new ProjectRequest(name, description, hackathonId, Set.of()));

		validate(new ProjectRequest(name, description, hackathonId, Set.of(1L)));
		validate(new ProjectRequest(name, description, hackathonId, Set.of(1L, 2L)));

	}

	private void validate(final ProjectRequest request)
	{
		projectValidatorService.validate(request);
	}

	private void testNpeIsThrown(final ProjectRequest request)
	{
		assertThrows(NullPointerException.class, () -> validate(request));
	}

	private void testProjectExceptionIsThrown(final ProjectRequest request)
	{
		assertThrows(ProjectException.class, () -> validate(request));
	}
}