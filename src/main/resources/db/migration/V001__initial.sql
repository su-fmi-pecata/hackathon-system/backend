CREATE OR REPLACE FUNCTION trigger_set_timestamp()
    RETURNS TRIGGER AS
$$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TABLE users
(
    id          SERIAL PRIMARY KEY,
    provider_id UUID        NOT NULL,
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE hackathons
(
    id         SERIAL PRIMARY KEY,
    name       VARCHAR(128) NOT NULL,
    location   VARCHAR(128) NOT NULL,
    start_date DATE         NOT NULL,
    end_date   DATE         NOT NULL,
    creator_id INTEGER      NOT NULL REFERENCES users(id),
    created_at TIMESTAMPTZ  NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ  NOT NULL DEFAULT NOW()
);
CREATE TRIGGER hackathons_update_row_timestamp_trigger
    BEFORE UPDATE
    ON hackathons
    FOR EACH ROW
EXECUTE FUNCTION trigger_set_timestamp();


CREATE TABLE projects
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(128) NOT NULL,
    description TEXT         NOT NULL,
    hackathon_id   INTEGER REFERENCES hackathons(id),
    creator_id  INTEGER      NOT NULL REFERENCES users(id),
    created_at  TIMESTAMPTZ  NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ  NOT NULL DEFAULT NOW()
);
CREATE TRIGGER projects_update_row_timestamp_trigger
    BEFORE UPDATE
    ON projects
    FOR EACH ROW
EXECUTE FUNCTION trigger_set_timestamp();

CREATE TABLE technologies
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(64) NOT NULL UNIQUE
);

INSERT INTO technologies(name)
VALUES ('c++'),
       ('java'),
       ('spring'),
       ('spring-boot'),
       ('angular'),
       ('react'),
       ('vue');

CREATE TABLE projects_technologies
(
    id         SERIAL PRIMARY KEY,
    project_id INTEGER NOT NULL REFERENCES projects(id),
    technology INTEGER NOT NULL REFERENCES technologies(id),
    UNIQUE (project_id, technology)
);

