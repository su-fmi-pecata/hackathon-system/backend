package com.hack.hackathon.user;

import com.hack.hackathon.user.model.User;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getByProviderId(final UUID providerId) {
        final Optional<User> user = userRepository.findByProviderId(providerId);
        return user.orElseGet(() -> persistUser(providerId));
    }

    private User persistUser(final UUID providerId) {
        final User user = User.builder().providerId(providerId).build();
        return userRepository.save(user);
    }
}
