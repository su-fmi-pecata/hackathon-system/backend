package com.hack.hackathon.user;

import com.hack.hackathon.user.model.User;
import com.hack.hackathon.user.role.RolePrefix;
import com.hack.hackathon.user.role.RoleService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserAuthenticationService {

    private final UserService userService;
    private final RoleService roleService;

    public UserAuthenticationService(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    public User getUser(final Authentication authentication)
    {
        final UUID providerId = UUID.fromString(authentication.getName());
        return userService.getByProviderId(providerId);
    }

    public boolean authorizeUser(RolePrefix rolePrefix, Long hackathonId, Authentication authentication) {
        final String requiredRole = roleService.getRole(rolePrefix, hackathonId);
        return authentication.getAuthorities()
                             .stream()
                             .map(GrantedAuthority::getAuthority)
                             .anyMatch(authority -> authority.equals(requiredRole));
    }
}
