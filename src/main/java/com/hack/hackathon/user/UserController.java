package com.hack.hackathon.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController
{

	@GetMapping("/session")
	public String session()
	{
		return "This endpoint is being used to get session and copy it from cookies";
	}
}
