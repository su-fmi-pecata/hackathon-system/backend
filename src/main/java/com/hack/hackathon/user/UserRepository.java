package com.hack.hackathon.user;

import com.hack.hackathon.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByProviderId(UUID providerId);
}
