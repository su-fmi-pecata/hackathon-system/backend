package com.hack.hackathon.user.role;

import com.hack.hackathon.identity.provider.KeycloakRestAdminClientService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RoleService {

    private final KeycloakRestAdminClientService keycloakRestAdminClientService;

    public RoleService(KeycloakRestAdminClientService keycloakRestAdminClientService) {
        this.keycloakRestAdminClientService = keycloakRestAdminClientService;
    }

    public void addRoleToUser(RolePrefix rolePrefix, Long id, UUID userId) {
        String role = getRole(rolePrefix, id);
        if(!keycloakRestAdminClientService.doesRoleExist(role)) {
            keycloakRestAdminClientService.createRealmRole(role);
        }
        keycloakRestAdminClientService.assignRoleToUser(userId, role);
    }

    public String getRole(RolePrefix rolePrefix, Long id) {
        return "%s_%d".formatted(rolePrefix.name(), id);
    }
}
