package com.hack.hackathon.project;

import com.hack.hackathon.project.model.ProjectRequest;
import com.hack.hackathon.project.model.ProjectResponse;
import com.hack.hackathon.project.service.ProjectService;
import com.hack.hackathon.user.UserAuthenticationService;
import com.hack.hackathon.user.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectController {

    private final ProjectService projectService;
    private final UserAuthenticationService userAuthenticationService;

    public ProjectController(ProjectService projectService, UserAuthenticationService userAuthenticationService) {
        this.projectService = projectService;
        this.userAuthenticationService = userAuthenticationService;
    }

    @GetMapping("/public/projects/{projectId}")
    public ProjectResponse getProjectById(@PathVariable Long projectId) {
        return ProjectResponse.convert(projectService.getProjectById(projectId));
    }

    @PostMapping("/projects")
    public ProjectResponse createProject(@RequestBody ProjectRequest projectRequest,
										 Authentication authentication)
    {
        final User user = userAuthenticationService.getUser(authentication);
        return ProjectResponse.convert(projectService.createProject(projectRequest, user));
    }

    @PutMapping("/projects/{projectId}")
    public ProjectResponse updateProject(@PathVariable Long projectId,
										 @RequestBody ProjectRequest projectRequest)
	{
        return ProjectResponse.convert((projectService.updateProject(projectRequest, projectId)));
    }
}
