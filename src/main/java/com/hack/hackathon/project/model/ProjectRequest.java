package com.hack.hackathon.project.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public record ProjectRequest(String name,
							 String description,
							 Long hackathonId,
							 Set<Long> technologies)
{

}
