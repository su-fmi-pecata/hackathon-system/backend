package com.hack.hackathon.project.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Set;

@Builder
@Getter
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ProjectResponse
{

    private Long id;

    private String name;

    private String description;

    private Long hackathonId;

    private Set<Long> technologies;

    public static ProjectResponse convert(Project project) {
        return ProjectResponse.builder()
                              .id(project.getId())
                              .name(project.getName())
                              .description(project.getDescription())
                              .hackathonId(project.getHackathonId())
                              .build();
    }

    public static List<ProjectResponse> convert(List<Project> projects) {
        return projects.stream()
                .map(ProjectResponse::convert)
                .toList();
    }
}
