package com.hack.hackathon.project.technology;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Builder(toBuilder = true)
@Getter
@Setter
@Table(name = "projects_technologies")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectTechnology {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long projectId;

    private Long technology;

    public static ProjectTechnology convertStringToTechnology(Long technologyId, Long projectId) {
        ProjectTechnology technology = ProjectTechnology.builder()
                .projectId(projectId)
                .technology(technologyId)
                .build();
        return technology;
    }
}
