package com.hack.hackathon.project.service;

import com.hack.hackathon.project.ProjectRepository;
import com.hack.hackathon.project.model.Project;
import com.hack.hackathon.project.model.ProjectRequest;
import com.hack.hackathon.project.technology.ProjectTechnology;
import com.hack.hackathon.project.technology.ProjectTechnologyRepository;
import com.hack.hackathon.user.model.User;
import com.hack.hackathon.user.role.RolePrefix;
import com.hack.hackathon.user.role.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService{

    private final ProjectRepository projectRepository;
    private final ProjectTechnologyRepository projectTechnologyRepository;
    private final ProjectValidatorService projectValidatorService;
    private final RoleService roleService;

    public ProjectServiceImpl(final ProjectRepository projectRepository,
                              final ProjectTechnologyRepository projectTechnologyRepository,
                              final ProjectValidatorService projectValidatorService,
                              final RoleService roleService) {
        this.projectRepository = projectRepository;
        this.projectTechnologyRepository = projectTechnologyRepository;
        this.projectValidatorService = projectValidatorService;
        this.roleService = roleService;
    }

    @Override
    public List<Project> getProjectsForHackathon(Long hackathonId) {
        return projectRepository.findAllByHackathonId(hackathonId);
    }

    @Override
    public Project getProjectById(Long projectId) {
        return projectRepository.findById(projectId).orElseThrow();
    }

    @Override
    public Project createProject(final ProjectRequest projectRequest,
                                 final User user) {

        projectValidatorService.validate(projectRequest);

        final Project project = Project.builder()
                                       .name(projectRequest.name())
                                       .description(projectRequest.description())
                                       .hackathonId(projectRequest.hackathonId())
                                       .creatorId(user.getId())
                                       .build();
        Project savedProject = projectRepository.saveAndFlush(project);

        saveTechnologies(projectRequest.technologies(), savedProject.getId());

        roleService.addRoleToUser(RolePrefix.PROJECT, savedProject.getId(), user.getProviderId());

        return savedProject;
    }

    @Override
    @Transactional
    public Project updateProject(final ProjectRequest projectRequest,
                                 final Long projectId) {

        projectValidatorService.validate(projectRequest);

        Project toBeUpdatedProject = projectRepository.findById(projectId).orElseThrow();
        toBeUpdatedProject.setId(projectId);
        toBeUpdatedProject.setName(projectRequest.name());
        toBeUpdatedProject.setDescription(projectRequest.description());
        projectRepository.save(toBeUpdatedProject);

        updateTechnologies(projectRequest.technologies(), projectId);

        return toBeUpdatedProject;
    }

    private void updateTechnologies(final Collection<Long> technologies,
                                    final Long projectId)
    {
        projectTechnologyRepository.deleteAllByProjectId(projectId);
        projectTechnologyRepository.flush();
        saveTechnologies(technologies, projectId);
    }

    private void saveTechnologies(Collection<Long> technologies, Long projectId) {
        List<ProjectTechnology> listTechnology = technologies.stream()
                .map(technologyId -> ProjectTechnology.builder().projectId(projectId).technology(technologyId).build())
                .toList();
        projectTechnologyRepository.saveAllAndFlush(listTechnology);
    }
}
