package com.hack.hackathon.project.service;

import com.hack.hackathon.project.ProjectException;
import com.hack.hackathon.project.model.ProjectRequest;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ProjectValidatorService
{

	public void validate(final ProjectRequest projectRequest)
	{
		Objects.requireNonNull(projectRequest.name(), "Name is required");
		Objects.requireNonNull(projectRequest.description(), "Description is required");
		Objects.requireNonNull(projectRequest.hackathonId(), "Hackathon id is required");
		Objects.requireNonNull(projectRequest.technologies(), "Technologies are required");

		if(projectRequest.technologies().isEmpty())
		{
			throw new ProjectException("At least one technology is required");
		}

	}

}
