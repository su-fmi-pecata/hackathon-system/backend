package com.hack.hackathon.project.service;

import com.hack.hackathon.project.model.Project;
import com.hack.hackathon.project.model.ProjectRequest;
import com.hack.hackathon.user.model.User;

import java.util.List;

public interface ProjectService {
    List<Project> getProjectsForHackathon(Long hackathonId);
    Project getProjectById(Long projectId);
    Project createProject(ProjectRequest projectRequest, User user);
    Project updateProject(ProjectRequest updatedProject, Long projectId);
}
