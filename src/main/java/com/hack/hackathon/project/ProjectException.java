package com.hack.hackathon.project;

public class ProjectException
		extends RuntimeException
{

	public ProjectException(final String message)
	{
		super(message);
	}
}
