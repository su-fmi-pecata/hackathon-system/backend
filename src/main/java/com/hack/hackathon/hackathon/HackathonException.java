package com.hack.hackathon.hackathon;

public class HackathonException
	extends RuntimeException
{

	public HackathonException(final String message)
	{
		super(message);
	}
}
