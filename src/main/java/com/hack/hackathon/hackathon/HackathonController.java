package com.hack.hackathon.hackathon;

import com.hack.hackathon.hackathon.model.Hackathon;
import com.hack.hackathon.hackathon.model.HackathonRequest;
import com.hack.hackathon.hackathon.model.HackathonResponse;
import com.hack.hackathon.hackathon.service.HackathonService;
import com.hack.hackathon.project.model.Project;
import com.hack.hackathon.project.model.ProjectResponse;
import com.hack.hackathon.project.service.ProjectService;
import com.hack.hackathon.user.UserAuthenticationService;
import com.hack.hackathon.user.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HackathonController {

    private final HackathonService hackathonService;
    private final ProjectService projectService;
    private final UserAuthenticationService userAuthenticationService;

    public HackathonController(HackathonService hackathonService,
                               ProjectService projectService,
                               UserAuthenticationService userAuthenticationService) {
        this.hackathonService = hackathonService;
        this.projectService = projectService;
        this.userAuthenticationService = userAuthenticationService;
    }

    @GetMapping("/public/hackathons")
    public List<HackathonResponse> getHackathons() {
        List<Hackathon> hackathons = hackathonService.getHackathons();
        return hackathons.stream()
                .map(HackathonResponse::convert)
                .toList();
    }

    @PostMapping("/hackathons")
    @ResponseStatus(HttpStatus.CREATED)
    public HackathonResponse createHackathon(@RequestBody HackathonRequest hackathonRequest,
                                             Authentication authentication) {
        final User user = userAuthenticationService.getUser(authentication);
        return HackathonResponse.convert(hackathonService.createHackathon(hackathonRequest, user));
    }

    @GetMapping("/public/hackathons/{hackathonId}")
    public HackathonResponse getHackathonById(@PathVariable Long hackathonId) {
        return HackathonResponse.convert(hackathonService.getHackathonById(hackathonId));
    }

    @PutMapping("/hackathons/{hackathonId}")
    public HackathonResponse updateHackathon(@PathVariable final Long hackathonId,
                                             @RequestBody final HackathonRequest hackathonRequest,
                                             final Authentication authentication) {
        final User user = userAuthenticationService.getUser(authentication);
        return HackathonResponse.convert(hackathonService.updateHackathon(hackathonRequest, hackathonId, user));
    }

    @GetMapping("/public/hackathons/{hackathonId}/projects")
    public List<ProjectResponse> getProjectsForHackathon(@PathVariable Long hackathonId) {
        List<Project> projects = projectService.getProjectsForHackathon(hackathonId);
        return ProjectResponse.convert(projects);
    }
}
