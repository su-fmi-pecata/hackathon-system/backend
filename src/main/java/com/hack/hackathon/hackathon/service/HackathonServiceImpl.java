package com.hack.hackathon.hackathon.service;

import com.hack.hackathon.hackathon.HackathonRepository;
import com.hack.hackathon.hackathon.model.Hackathon;
import com.hack.hackathon.hackathon.model.HackathonRequest;
import com.hack.hackathon.user.model.User;
import com.hack.hackathon.user.role.RolePrefix;
import com.hack.hackathon.user.role.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HackathonServiceImpl implements HackathonService {

    private final HackathonRepository hackathonRepository;
    private final HackathonValidatorService hackathonValidatorService;
    private final RoleService roleService;

    public HackathonServiceImpl(final HackathonRepository hackathonRepository,
                                final HackathonValidatorService hackathonValidatorService,
                                final RoleService roleService) {
        this.hackathonRepository = hackathonRepository;
        this.hackathonValidatorService = hackathonValidatorService;
        this.roleService = roleService;
    }

    @Override
    public List<Hackathon> getHackathons() {
        return hackathonRepository.findAll();
    }

    @Override
    public Hackathon getHackathonById(Long hackathonId) {
        return hackathonRepository.findById(hackathonId).orElseThrow();
    }

    @Override
    public Hackathon createHackathon(HackathonRequest hackathonRequest, User user) {
        hackathonValidatorService.validate(hackathonRequest);

        Hackathon hackathon = Hackathon.builder()
                .name(hackathonRequest.name())
                .location(hackathonRequest.location())
                .startDate(hackathonRequest.startDate())
                .endDate(hackathonRequest.endDate())
                .creatorId(user.getId())
                .build();

        hackathon = hackathonRepository.saveAndFlush(hackathon);
        roleService.addRoleToUser(RolePrefix.HACKATHON, hackathon.getId(), user.getProviderId());
        return hackathon;
    }

    @Override
    public Hackathon updateHackathon(final HackathonRequest hackathonRequest,
                                     final Long hackathonId,
                                     final User user) {

        hackathonValidatorService.validate(hackathonRequest);

        Hackathon toBeUpdatedHackathon = hackathonRepository.findById(hackathonId).orElseThrow();
        toBeUpdatedHackathon.setName(hackathonRequest.name());
        toBeUpdatedHackathon.setLocation(hackathonRequest.location());
        toBeUpdatedHackathon.setStartDate(hackathonRequest.startDate());
        toBeUpdatedHackathon.setEndDate(hackathonRequest.endDate());
        toBeUpdatedHackathon.setCreatorId(user.getId());
        return hackathonRepository.saveAndFlush(toBeUpdatedHackathon);
    }
}
