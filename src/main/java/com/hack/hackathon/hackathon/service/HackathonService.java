package com.hack.hackathon.hackathon.service;

import com.hack.hackathon.hackathon.model.Hackathon;
import com.hack.hackathon.hackathon.model.HackathonRequest;
import com.hack.hackathon.user.model.User;

import java.util.List;

public interface HackathonService {
    
    List<Hackathon> getHackathons();
    Hackathon createHackathon(HackathonRequest hackathonRequest, User user);
    Hackathon getHackathonById(Long hackathonId);
    Hackathon updateHackathon(HackathonRequest hackathonRequest, Long hackathonId, User user);
}
