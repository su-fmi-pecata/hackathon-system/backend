package com.hack.hackathon.hackathon.service;

import com.hack.hackathon.hackathon.HackathonException;
import com.hack.hackathon.hackathon.model.HackathonRequest;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class HackathonValidatorService
{

	public void validate(final HackathonRequest hackathonRequest)
	{
		Objects.requireNonNull(hackathonRequest.name(), "Name is required");
		Objects.requireNonNull(hackathonRequest.location(), "Location is required");
		Objects.requireNonNull(hackathonRequest.startDate(), "Start Date is required");
		Objects.requireNonNull(hackathonRequest.endDate(), "End Date is required");

		if (hackathonRequest.endDate().isBefore(hackathonRequest.startDate()))
		{
			throw new HackathonException("Start date is after end date!");
		}
	}
}
