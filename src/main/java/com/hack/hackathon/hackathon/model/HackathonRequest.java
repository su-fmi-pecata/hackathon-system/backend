package com.hack.hackathon.hackathon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public record HackathonRequest(String name,
							   String location,
							   LocalDateTime startDate,
							   LocalDateTime endDate)
{

}
