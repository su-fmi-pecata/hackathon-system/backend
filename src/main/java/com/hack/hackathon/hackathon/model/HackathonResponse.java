package com.hack.hackathon.hackathon.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@Getter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class HackathonResponse
{

    private final Long id;

    private final String name;

    private final String location;

    private final LocalDateTime startDate;

    private final LocalDateTime endDate;

    private final Long creatorId;

    public static HackathonResponse convert(Hackathon hackathon) {
        return HackathonResponse.builder()
                                .id(hackathon.getId())
                                .name(hackathon.getName())
                                .location(hackathon.getLocation())
                                .startDate(hackathon.getStartDate())
                                .endDate(hackathon.getEndDate())
                                .creatorId(hackathon.getCreatorId())
                                .build();
    }
}
