package com.hack.hackathon.config;

import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

// https://appsdeveloperblog.com/guide-to-use-keycloak-with-spring-boot/
@KeycloakConfiguration
public class SecurityConfig
		extends KeycloakWebSecurityConfigurerAdapter
{

	@Autowired
	public void configureGlobal(final AuthenticationManagerBuilder authenticationManagerBuilder)
	{
		final SimpleAuthorityMapper grantedAuthorityMapper = new SimpleAuthorityMapper();
		grantedAuthorityMapper.setPrefix("");

		final KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
		keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(grantedAuthorityMapper);
		authenticationManagerBuilder.authenticationProvider(keycloakAuthenticationProvider);
	}

	@Bean
	@Override
	protected SessionAuthenticationStrategy sessionAuthenticationStrategy()
	{
		return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
	}

	/**
	 * super.configure(http); -> Configuration which keycloak is providing, requires authentication for
	 * all `DELETE`, `PATCH`, `POST` and `PUT` methods, which cannot be overridden. Consider this when
	 * adding new endpoints for security.
	 */
	@Override
	protected void configure(final HttpSecurity http)
			throws Exception
	{
		super.configure(http);
		http.cors().and()
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html").permitAll()
			.antMatchers(HttpMethod.GET, "/public/**").permitAll()
			.antMatchers(HttpMethod.PUT, "/hackathons/{hackathonId}").access("@userAuthenticationService.authorizeUser('HACKATHON', #hackathonId, authentication)")
			.antMatchers(HttpMethod.PUT, "/projects/{projectId}").access("@userAuthenticationService.authorizeUser('PROJECT', #projectId, authentication)")
			.anyRequest().authenticated()
		;
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource()
	{
		final CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(List.of("*")); // TODO: Specify allowed domains
		configuration.setAllowedMethods(List.of(GET.name(), POST.name(), PUT.name(), DELETE.name()));
		configuration.setAllowedHeaders(List.of("origin", "content-type", "accept", "x-requested-with", "Authorization"));
		configuration.setMaxAge(60L);

		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}
