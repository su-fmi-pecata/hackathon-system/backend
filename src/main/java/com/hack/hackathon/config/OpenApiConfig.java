package com.hack.hackathon.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Hackathon System API", version = "v1"))
@SecurityScheme(
		name = "OAuth_Token_Request_State",
		in = SecuritySchemeIn.COOKIE,
		type = SecuritySchemeType.HTTP,
		scheme = "bearer"
)
public class OpenApiConfig
{

}
