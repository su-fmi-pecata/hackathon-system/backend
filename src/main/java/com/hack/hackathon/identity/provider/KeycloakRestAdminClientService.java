package com.hack.hackathon.identity.provider;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
public class KeycloakRestAdminClientService
{
	private final RealmResource manageableRealm;

	public KeycloakRestAdminClientService(@Value("${keycloak-admin.realm}") final String adminRealm,
										  @Value("${keycloak-admin.username}") final String adminUsername,
										  @Value("${keycloak-admin.password}") final String adminPassword,
										  @Value("${keycloak-admin.clientId}") final String adminClientId,
										  @Value("${keycloak.auth-server-url}") final String serverUrl,
										  @Value("${keycloak.realm}") final String manageableRealmName)
	{
		final ResteasyClient connectionPool = new ResteasyClientBuilder().connectionPoolSize(10).build();

		this.manageableRealm = KeycloakBuilder.builder()
				.serverUrl(serverUrl)
				.realm(adminRealm)
				.username(adminUsername)
				.password(adminPassword)
				.clientId(adminClientId)
				.resteasyClient(connectionPool)
				.build()
				.realm(manageableRealmName);
	}

	public void createRealmRole(String roleName) {
		RoleRepresentation roleRep = new  RoleRepresentation();
		roleRep.setName(roleName);
		manageableRealm.roles().create(roleRep);
	}

	public void assignRoleToUser(UUID userId, String role) {
		RoleRepresentation newRoleRep = manageableRealm.roles().get(role).toRepresentation();
		manageableRealm.users().get(userId.toString()).roles().realmLevel().add(Collections.singletonList(newRoleRep));
	}

	public boolean doesRoleExist(String roleName) {
		try {
			manageableRealm.roles().get(roleName).toRepresentation();
			return true;
		} catch (Exception ignored) {

		}
		return false;
	}
}
