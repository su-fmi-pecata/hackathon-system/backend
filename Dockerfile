FROM openjdk:18-alpine3.15

COPY build/libs/app.jar /

ENTRYPOINT exec java $JAVA_OPTS -jar /app.jar
